import Customer
import math

#creates shopping cart object and function
class ShoppingCart(object):
    def __init__(self, cartid):
        self.cartid = cartid

    def calcquantity(self,list): #calculates the quantity of times in the shopping cart
        quantity = len(list)
        return quantity

    def calcNetTotal(self,list):  #calculates the purchase price before discount, and checks for less than 50 items
        quantity = len(list)
        netTotal= 0


        if (quantity < 50 ):
            for item in list:
                netTotal += item.price
                quantity += 1
                netTotal = self.roundTraditional(netTotal,2)

        else:
            print("You may not purchase more than 50 items.")

            #may need a try and catch conditional insted of if else

        print("Your purchase price before discounts:", netTotal)
        return netTotal


#
    def roundTraditional(self,val,digits):
        rtd = (val*1000) % 10
        if (rtd >= 5) :
            rounded = math.ceil(val*100)/100


        else: rounded = math.floor(val*100)/100


        return rounded
        #print(rounded)

    def calcQuantityDiscount(self, quantity, netTotal): #calculates discounts based on quantity
        if (quantity > 9):
            discountTotal = netTotal * .10
            newTotal = netTotal - discountTotal
            print("Your quantity discount is", discountTotal)
            return newTotal

        elif(quantity > 4 & quantity <10):
            discountTotal = netTotal *.05
            newTotal = netTotal - discountTotal
            print("Your quantity discount is", discountTotal)
            return newTotal

        else:
            discountTotal = 0
            newTotal = netTotal
            print("Your quantity discount is", discountTotal)
            return newTotal

    def custDiscountTax(self, customer, newTotal): #calculates price after cust loyalty discount and taxes
        if(customer.loyalty == True and customer.tax== True): #a loyalty member, and not tax exempt
            cdiscount = self.custLoyalty(newTotal)
            afterLoyalty= newTotal - cdiscount
            total = afterLoyalty + self.taxRate(afterLoyalty)
            total = str(self.roundTraditional(total, 2))
            tax = self.taxRate(newTotal)
            print("Your customer discount is",cdiscount )


        elif(customer.loyalty== True and customer.tax== False): # a loyalty member, tax exempt
            cdiscount = self.custLoyalty(newTotal)
            afterLoyalty = newTotal - cdiscount
            total = afterLoyalty #+ self.taxRate(afterLoyalty) Tax exempt
            total = str(self.roundTraditional(total, 2))
            print("Your customer discount is", cdiscount)
            tax = 0


        elif(customer.loyalty== False and customer.tax== True): #not a loyalty member, and not tax exempt
            afterLoyalty = newTotal
            total = afterLoyalty + self.taxRate(newTotal)
            total = str(self.roundTraditional(total, 2))
            tax = self.taxRate(newTotal)
            #return total

        else:# not a loyalty memeber, and tax exempt
            total = self.roundTraditional(newTotal, 2)
            tax = 0

        print("Your total tax is", tax)
        print("Your final total is", total)
        return total

    def custLoyalty(self, sum):
        custDiscount = sum * .90
        custDiscount = sum - custDiscount
        return custDiscount


    def taxRate(self,total): #calculates standard tax rate
        tax = self.roundTraditional(total, 2)
        tax= tax * .045
        return tax

