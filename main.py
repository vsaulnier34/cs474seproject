from Customer import Customer
from Customer import Item
from ShoppingCart import ShoppingCart

ob1 = Item(20, 35.11)
ob2 = Item(30, 55.10)
ob3 = Item(40, 12.15)
ob4 = Item(50, 13.75)
ob5 = Item(60, 25.16)

purchaseList = [ob1, ob2, ob3]
customer1 = Customer('Bill', True, True)

purchaseList2 = [ob1, ob2, ob3, ob4, ob5]

customer2= Customer('Jane', False, False)

quantity = ShoppingCart(1).calcquantity(purchaseList)
netTotal = ShoppingCart(1).calcNetTotal(purchaseList)
discount = ShoppingCart(1).calcQuantityDiscount(quantity,
                                         netTotal)
finalTotal = ShoppingCart(1).custDiscountTax(customer1, discount)

quantity2 = ShoppingCart(2).calcquantity(purchaseList2)
netTotal2 = ShoppingCart(2).calcNetTotal(purchaseList2)
discount2 = ShoppingCart(2).calcQuantityDiscount(quantity2,
                                         netTotal2)

finalTotal2 = ShoppingCart(2).custDiscountTax(customer2, discount2)






