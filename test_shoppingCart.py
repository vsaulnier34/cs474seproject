from unittest import TestCase
from ShoppingCart import ShoppingCart
from Customer import Item
from Customer import Customer

class TestShoppingCart(TestCase):
    #def setUp(self):
     #   self.ShoppingCart = ShoppingCart(4)

    def test_calcNetTotal(self):
        s= ShoppingCart(7)
        ob1 = Item(20, 35.11)
        ob2 = Item(30, 55.10)
        ob3 = Item(40, 12.15)
        ob4 = Item(50, 13.75)
        ob5 = Item(60, 25.16)

        purchaseList = [ob1, ob2, ob3, ob4, ob5]
        nt = ShoppingCart.calcNetTotal(s,purchaseList)
        self.assertEqual(nt,141.27)
        #self.fail()

    def test_roundTraditional(self):
        s= ShoppingCart(7)

        r = ShoppingCart.roundTraditional(s,13.764, 2)
        t = ShoppingCart.roundTraditional(s, 13.765, 2)
        u = ShoppingCart.roundTraditional(s, 13.767, 2)
        o = ShoppingCart.roundTraditional(s, 13.760, 2)
        v = ShoppingCart.roundTraditional(s,13.679,2)
       # x = ShoppingCart.roundTraditional(s,13.76499,2)

        # 13.7649 edge case to add
        #13.76499 edge case to add
        self.assertEqual(r, 13.76)
        self.assertEqual(t,13.77)
        self.assertEqual(u,13.77)
        self.assertEqual(o,13.76)
        self.assertEqual(v,13.68)
       # self.assertEqual(x,13.77)

        #self.fail()
    #
    def test_calcQuantityTenDiscount(self):
        s = ShoppingCart(7)
        netTotal = 100
        listTenItems = ('eggs', 'cheese', 'apple', 'milk', 'bread', 'beans', 'bacon', 'bananas', 'mushrooms', 'oj')
        listTenQuantity = len(listTenItems)
        disc10 = ShoppingCart.calcQuantityDiscount(s, listTenQuantity, netTotal)
        self.assertEqual(90, disc10)
    #     self.fail()

    def test_calcQuantityFiveToNineDiscount(self):
        s = ShoppingCart(7)
        netTotal = 100
        listSixItems = ('eggs', 'cheese', 'apple', 'milk', 'bread', 'beans')
        listSixQuantity = len(listSixItems)
        disc5 = ShoppingCart.calcQuantityDiscount(s,listSixQuantity,netTotal)
        self.assertEqual(95,disc5)


    def test_calcQuantityNoDiscount(self):
        s = ShoppingCart(7)
        netTotal = 100
        listFourItems = ('eggs', 'cheese', 'apple', 'milk')
        listFourQuantity = len(listFourItems)
        noDisc  = ShoppingCart.calcQuantityDiscount(s, listFourQuantity, netTotal)
        self.assertEqual(100, noDisc)


    def test_custDiscountTax(self):
        #test to check the discounts and taxes are being applied correctly
        s = ShoppingCart(7)
        grossTotal = 100
        custBothTrue = Customer("John", True, True)

        bothTotalAfter = s.custDiscountTax(custBothTrue,grossTotal)
        self.assertEqual('94.05',bothTotalAfter) #94.05 = (100*0.9)(1.045)

        custLoyaltyTrue = Customer('Jim',True,False)
        loyaltyTotalAfter = s.custDiscountTax(custLoyaltyTrue,grossTotal)
        self.assertEqual('90.0',loyaltyTotalAfter)


        custTaxTrue = Customer("James",False,True)
        loyaltyTotalAfter = s.custDiscountTax(custTaxTrue,grossTotal)
        self.assertEqual('104.5',loyaltyTotalAfter)


        custNeitherTrue = Customer("Frank",False,False)
        self.assertEqual(100.0,s.custDiscountTax(custNeitherTrue,grossTotal))


    def test_custLoyalty(self):
    #test to check if the customer loyalty calculation is returning the correct value
        s = ShoppingCart(2)
        total = 100
        self.assertEqual(10.0, ShoppingCart.custLoyalty(s,total))

    def test_taxRate(self):
        # test to check if the tax rate calculation is returning the correct value
        s= ShoppingCart(3)
        total =100
        self.assertEqual(4.5, ShoppingCart.taxRate(s,total))